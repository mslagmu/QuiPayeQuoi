class PageManger {
    constructor() {
        this.Pages = new Map();
    }

    register(name,vue) {
        this.Pages.set(name,vue);
    }

    getVue(name ) {
        return this.Pages.get(name);
    }

    show(name) {
        this.Pages.forEach((p,n) => {
            p.Visible = 0;
            if (n==name) {
                p.Visible = 1;
            }
        })
    }
}


function getId(max) {
    r= ""
    for (i=0;i < max ; i++) {
      digit =  Math.floor(Math.random()*36);
      
      if ( digit < 10 ) {
          digit += 48;
      } else {
         digit += 55;
      }
      
      r = r + String.fromCharCode(digit);
    }
    return r
}


Vue.component("form-button", {
    props : ["content","color"],
    data () {
        return {
            classval : "w3-button w3-btn w3-tiny w3-"+this.color
        }
    },
    template : `<button :class="classval" @click="$emit('click')">{{content}}</button>`    
})

//html

Vue.component("tab",{
    props : ["active"],
    data () {
        return { }
    },
    computed : {
        classval() {
            if  ( this.active ) {
                return "w3-btn w3-bar-item w3-blue";
            } else {
                return "w3-btn w3-bar-item";
            }
        }
    },

    template : `<button @click="$emit('click')" :class="classval" ><slot></slot></button>`
})


Vue.component("form-dialog",{
    props : ['title'],
    template : /*html*/ `
    <div  class="w3-modal"  style="display:block">
        <div class="w3-modal-content">
            <div class="w3-container" >
                <div class="w3-container w3-red">
                        <h1>{{title}}</h1>
                </div>
                    <slot></slot>
                <div>
                        <form-button content="OK" color="red" @click="$emit('close')"></form-button>  
                </div>
        
            </div>
        </div>
    </div>
    `,
})


function init1() {
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
        .register('sw.js', { scope: '../pif' })
        .then(function(reg) {
            console.log("sw ok !!")
        });
    }
    Notification.requestPermission((permission) => {
        if (Notification.permission === "granted") {
            console.log("Notification OK")
            navigator.serviceWorker.ready.then(function(registration) {
                console.log("Ready OK")
                registration.pushManager.subscribe({"userVisibleOnly":true}).then((subscription)=>{
                    console.log("text:",subscription)
                }).catch(error => {
                    console.log("error:", error);
                })
            })
            
        }
    })




}

var pm = new PageManger();


function init() {

    
    
    pm.register("compte",initCompte());
    pm.register("liste",initListe());

    fetch("list.json").then(rep=>{
        return rep.json().then(body => {
            pm.getVue("liste").comptes = body;
            
        })
    }).catch()  
}


function initListe() {
    liste = new Vue({
        el: "#liste",
        data: {
            comptes: [],
            Visible: 1,
            currentCompte: -1,
        },
        methods: {
            "chargercompte": function (i) {
                fetch(this.comptes[i].url).then(rep => {
                    return rep.json().then(body => {
                        pm.getVue("compte").count = body;
                        pm.show("compte");
                        pm.getVue("compte").calculbalance();
                    });
                }).catch();
            },
            "modifierCompte": function (i) {
                this.currentCompte = i;
            },
            "suprimerCompte": function (i) {
                this.comptes.splice(i, 1);
            },
            "ajoutCompte": function () {
                var c = {
                    nom: "",
                    url: getId(10)
                };
                var l = this.comptes.push(c);
                this.currentCompte = l - 1;
            },
        }
    });
    return liste
}

function show(s) {
    pm.show(s);
}



function initCompte() {
    compte = new Vue({
        el: '#count',
        data: {
            count: {},
            "utilisateur": -1,
            "newParticipant": "",
            "DepenseTotale": 0,
            "DepensePart": 0,
            "Visible" : 0,
            "currentUser" : -1,
            "currentDepense" : -1,

            "tableau" : "participants"
        },
        methods: {
            "ajoutParticipant": function () {
                var part = { "Nom": "", "Part": 1, "Balance": 0 };
               var l =  this.count["participants"].push(part);
               this.currentUser = l-1;
            },
            "suprimerParticipant": function (i) {
                this.count["participants"].splice(i, 1);
            },
            "modifierParticipant" : function (i) {
                this.currentUser=i;
    
            },
            "ajoutDepense": function () {
                var dep = {
                    "Libelle": "",
                    "Montant": 0,
                    "Participant": this.utilisateur
                };
                var l = this.count["depenses"].push(dep);
                this.currentDepense=l-1;
                
            },
            "modifierDepense" : function (i) {
                this.currentDepense=i;
            },


            "suprimerDepense": function (i) {
                this.count["depenses"].splice(i, 1);
                this.calculbalance();
            },
            "calculbalance": function () {
                var total = 0;
                var totalpar = [];
                var part = 0;
                var depensepart = 0;
                this.count.participants.forEach(p => {
                    totalpar.push(0);
                    part += p.Part;
                });
                for (p of this.count.depenses) {
                    total += p.Montant;
                    totalpar[p.Participant] += p.Montant;
                }
                var depensepart = total / part;
                this.DepensePart = depensepart;
                this.DepenseTotale = total;
                this.count.participants.forEach((p, i) => {
                    var balance = totalpar[i] - depensepart * p.Part;
                    p.Balance = balance;
                    
                });
            },
            balancecolor(i) {
                if (this.count.participants[i].Balance  < 0 ) {
                    return "red"
                } else {
                    return "green"
                }
            }
        },
        computed : {
            buttoncolor : function(s) {
               return s=this.tableau;
            }
        }
    });
    return compte;
}
