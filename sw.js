var localcache ;

async function match(request) {
    const r = await caches.match(request)
    if (r) return r
    return fetch(request)
}


this.addEventListener('fetch1', function(event) {
    event.respondWith(
        match(event.request)
  )
});


addEventListener('fetch', event => {
    // Prevent the default, and handle the request ourselves.
    event.respondWith(async function() {
      // Try to get the response from a cache.
      const cachedResponse = await caches.match(event.request);
      // Return it if we found one.
      if (cachedResponse) return cachedResponse;
      // If we didn't find a match in the cache, use the network.
      return fetch(event.request);
    }());
  });


  this.addEventListener('install1', function(event) {
        caches.open("maison").then(function(cache){
            localcache = cache;
            localcache.put('datas', new Response("michel"));
        })

        

  });


  this.addEventListener('install', function(event) {
    caches.open("maison").then(function(cache){
        localcache = cache;
        localcache.put('datas', new Response("michel"));
    })

    

});